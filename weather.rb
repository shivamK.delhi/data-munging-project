# frozen_string_literal: true

weather_data = File.open('./weather.dat', File::RDONLY, &:read)

weather_data_array = weather_data.lines.map { |l| l.split.map(&:to_f) }

weather_data_array.delete_if { |data| data == [] || data[0].zero? }

t_spread = weather_data_array[0][1] - weather_data_array[0][2]

day = weather_data_array[0][0]

weather_data_array.each do |row|
  curr_tspread = row[1] - row[2]
  if t_spread > curr_tspread
    t_spread = curr_tspread
    day = row[0]
  end
end

p day.to_i
