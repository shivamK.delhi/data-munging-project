# frozen_string_literal: true

football = File.open('./football.dat', File::RDONLY, &:read)

football_array = football.lines.map(&:split)

football_array.delete_at(0)

diff = (football_array[0][-4].to_i - football_array[0][-2].to_i).abs
name = football_array[0][1]

football_array.each do |row|
  if (row[-4].to_i - row[-2].to_i).abs < diff
    diff = (row[-4].to_i - row[-2].to_i).abs
    name = row[1]
  end
end

p name
